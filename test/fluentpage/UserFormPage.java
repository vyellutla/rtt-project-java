package fluentpage;

import org.fluentlenium.core.FluentPage;
import org.openqa.selenium.WebDriver;

import static org.fluentlenium.core.filter.FilterConstructor.withId;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

/**
 * Index Page TEsting for formvalidation App
 *
 */
public class UserFormPage extends FluentPage {
    private String url;

    public UserFormPage(WebDriver webDriver, int port) {
        super(webDriver);
        this.url = "http://localhost:" + port+"/user";
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public void isAt() {
        assert(title().equals("UserForm"));
    }

    public void submitForm(String email, String password) {
        // Fill the input field with id "name" with the passed name string.
        fill("#email").with(email);
        fill("#password").with(password);
        //Submit the form whose id is "submit"
        submit("#submit");
    }
}
