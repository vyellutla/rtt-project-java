# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table USERS (
  USER_ID                   bigint not null,
  USERNAME                  varchar(16) not null,
  EMAIL                     varchar(255) not null,
  OBJ_VERSION               integer not null,
  constraint uq_USERS_USERNAME unique (USERNAME),
  constraint pk_USERS primary key (USER_ID))
;

create sequence USERS_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists USERS;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists USERS_seq;

