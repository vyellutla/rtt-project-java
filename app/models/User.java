package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;

/**
 * User for rtt-project
 *
 */

@Entity
@Table(name = "USERS")
public class User extends Model {
    @Id @GeneratedValue
    @Column(name = "USER_ID")
    private Long id = null;

    @Version
    @Column(name = "OBJ_VERSION")
    private int version = 0;

    @Column(name = "USERNAME", length = 16, nullable = false, unique = true)
    private String userName; // Unique and immutable

    @Constraints.Email
    @Column(name = "EMAIL", length = 255, nullable = false)
    private String email;


    // ********************** Getter/Setter Methods ********************** //
    public Long getId() {  return id;    }
    public void setId(Long id) {   this.id = id;    }
    public int getVersion() {  return version;     }
    public void setVersion(int version) { this.version = version;     }
    public String getUserName() {  return userName;     }
    public void setUsername(String username) { this.userName = username;    }
    public String getEmail() {  return email;     }
    public void setEmail(String email) { this.email = email;    }
}
