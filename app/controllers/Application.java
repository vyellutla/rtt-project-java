package controllers;

import com.google.common.base.Preconditions;
import models.User;

import play.data.Form;
import play.mvc.*;
import static play.data.Form.*;

import views.html.index;
import views.html.user.*;

public class Application   extends Controller {
        final static Form<User> userForm=form(User.class);
        public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result blank() {
        return ok(form.render(userForm,"(None)","(None)"));
    }

    public static Result submit(){
        User user;
        Form<User> filledUserForm= userForm.bindFromRequest();
        Preconditions.checkNotNull(filledUserForm);
        if(filledUserForm.hasErrors()) {
            return badRequest(form.render(filledUserForm,"(None)","(None)"));
        }else{

            user= filledUserForm.get();
        }
        return ok(form.render(userForm, user.getUserName(),user.getEmail()));

    }




}
